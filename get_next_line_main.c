#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#include "get_next_line.h"

int		main()
{
	int 	ret = 0;
	int		fd00;
	int		fd01;
//	int		fd02;
	char	*line = 0;

	fd01 = open("./tests/test01.txt", O_RDWR | O_APPEND);
	if (fd01 == -1)
		return (0);

	while ((ret = get_next_line(fd01, &line) > 0))
	{
		printf("output: %s\n", line);
		printf("ret: %d\n", ret);
		free(line);
	}
//	free(line);
	close(fd01);

	fd00 = open("./tests/test00.txt", O_RDWR | O_APPEND);
	if (fd00 == -1)
		return (0);

	while ((ret = get_next_line(fd00, &line) > 0))
	{
		printf("output: %s\n", line);
		printf("ret: %d\n", ret);
		free(line);
	}
//	free(line);
	close(fd00);


/*
	close(fd01);
	line = malloc(sizeof(char *));
	fd00 = open("./tests/test00.txt", O_RDWR | O_APPEND);
	while (get_next_line(fd00, line))
	{
		printf("\033[1;31m");
		printf("main output %s\n", line[0]);
		printf("............................\n");
		printf("\033[0m");
	}
	free(line);

	line = malloc(sizeof(char *));
	fd01 = open("./tests/test01.txt", O_RDWR | O_APPEND);
	while (get_next_line(fd01, line))
	{
		printf("\033[1;31m");
		printf("main output %s\n", line[0]);
		printf("............................\n");
		printf("\033[0m");
	}
	free(line);

	line = malloc(sizeof(char *));
	fd02 = open("./tests/test02.txt", O_RDWR | O_APPEND);
	while (get_next_line(fd02, line))
	{
		printf("\033[1;31m");
		printf("main output %s\n", line[0]);
		printf("............................\n");
		printf("\033[0m");
	}
	free(line);


	close(fd00);
	close(fd01);
	close(fd02);

	line = malloc(sizeof(char *));
	fd00 = open("./tests/test00.txt", O_RDWR | O_APPEND);
	while (get_next_line(fd00, line))
	{
		printf("\033[1;31m");
		printf("main output %s\n", line[0]);
		printf("............................\n");
		printf("\033[0m");
	}
	free(line);
	close(fd00);

*/

}
