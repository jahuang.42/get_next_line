#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include "get_next_line.h"

static t_list	*get_or_create_node(t_list **static_list, int fd)
{
	t_list		*current;
	t_list		*last;

	current = *static_list;
	last = NULL;
	while (current)
	{
		if (current->fd == fd)
			return (current);
		last = current;
		current = current->next;
	}
	current = (t_list *)malloc(sizeof(t_list));;
	if (!current)
		return (NULL);
	current->content = (char *)malloc(sizeof(char) * 1);
	current->fd = fd;
	current->next = NULL;
	if (!(*static_list))
		*static_list = current;
	else
		last->next = current;
	return (current);
}

void			delete_node(t_list **node, int fd)
{
	t_list		*to_free;
	t_list		*last;

	to_free = *node;
	last = NULL;
	while (to_free->fd != fd)
	{
		if (!to_free->next)
			break ;
		last = to_free;
		to_free = to_free->next;
	}
	if (last)
		last->next = to_free->next;
	else
		*node = to_free->next;
	if (to_free->content)
	{
		free(to_free->content);
		to_free->content = NULL;
	}
	if (to_free)
		free(to_free);
	*node = NULL;
}

int					get_next_line(int fd, char **line)
{
	static t_list	*static_list;
	t_list			*fd_node;
	int				ret;
	char			buf[BUFFER_SIZE + 1];
	int				nl_index;

	fd_node = get_or_create_node(&static_list, fd);
	while ((nl_index = find_n(fd_node->content)) == -1 && (ret = read(fd, buf, BUFFER_SIZE)) > 0)
	{
		buf[ret] = '\0';
		fd_node->content = join(&(fd_node->content), buf);
	}
	if (nl_index >= 0)
	{
		*line = get_line(fd_node->content, nl_index);
		fd_node->content = get_remain(fd_node->content, nl_index + 1);
		return (1);
	}
	if (!fd_node->content && ret <= 0)
		delete_node(&static_list, fd);
	printf("ret: %d\n",ret);
	return (ret + nl_index);
}
