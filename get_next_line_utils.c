#include "get_next_line.h"
#include <stdio.h>

int					ft_strlen(char *s)
{
	int				index;

	index = 0;
	while (s[index])
		index++;
	return (index);
}

char				*join(char **content, char *buf)
{
	int				index;
	char			*new_content;
	char			*holder;
	int				content_len;
	int				buf_len;

	index = 0;
	holder = *content;
	content_len = ft_strlen(holder);
	buf_len = ft_strlen(buf);
	new_content = malloc(sizeof(char) * (buf_len + content_len + 1));
	if (!new_content)
		return (NULL);
	while (index < (content_len + buf_len))
	{
		if (index < content_len)
			new_content[index] = holder[index];
		else
			new_content[index] = buf[index - content_len];
		index++;
	}
	new_content[index] = '\0';
	if(*content)
		free(*content);
	return (new_content);
}

int					find_n(char *s)
{
	int				index;

	index = 0;
	if (!s)
		return (-1);
	while (s[index])
	{
		if(s[index] == '\n')
			return (index);
		index++;
	}
	return (-1);
}

char			*get_remain(char *s, int start)
{
	int			index;
	int			len;
	char		*result;
	
	index = 0;
	result = NULL;
	len = ft_strlen(s) - start;
	result = (char *)malloc(sizeof(char) * (len + 1));
	if (!result)
		return (NULL);
	while (s[index + start])
	{
		result[index] = s[index + start];
		index++;
	}
	result[index] = '\0';
	if(s)
		free(s);
	return (result);
}

char		*get_line(char *s, int end)
{
	int			index;
	char		*result;
	
	index = 0;
	result = (char *)malloc(sizeof(char) * (end + 1));
	if (!result)
		return (NULL);
	while (index < end)
	{
		result[index] = s[index];
		index++;
	}
	result[index] = '\0';
	return (result);
}
