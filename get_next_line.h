#ifndef		GET_NEXT_LINE_H
# define	GET_NEXT_LINE_H

# include <stdlib.h>
# include <stddef.h>

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 10
# endif

typedef struct		s_list
{
	int				fd;
	char			*content;
	struct s_list	*next;
}					t_list;

int					get_next_line(int fd, char **line);
int					ft_strlen(char *s);
char				*join(char **content, char *buf);
int					find_n(char *s);
char				*get_remain(char *s, int start);
char				*get_line(char *s, int end);

#endif
